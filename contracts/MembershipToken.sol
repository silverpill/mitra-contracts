// SPDX-License-Identifier: GPL-3.0

pragma solidity ^0.8.0;

import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/token/ERC721/ERC721.sol";
import "@openzeppelin/contracts/utils/Counters.sol";

/**
 * @dev Example of membership token contract.
 * This contract is supposed to be controlled by instance administrator.
 */
contract MembershipToken is ERC721, Ownable {
    using Counters for Counters.Counter;

    Counters.Counter private _tokenIds;

    constructor(
        string memory name,
        string memory symbol
    ) ERC721(name, symbol) {
    }

    function mint(address to)
        public
        onlyOwner
    {
        require(
            balanceOf(to) == 0,
            "MembershipToken: token already minted"
        );

        _tokenIds.increment();
        uint256 tokenId = _tokenIds.current();
        _safeMint(to, tokenId);
    }

    function _transfer(address, address, uint256)
        internal
        pure
        override
    {
        revert("MembershipToken: transfers are not allowed");
    }
}
