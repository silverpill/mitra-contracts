// SPDX-License-Identifier: GPL-3.0

pragma solidity ^0.8.0;

import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/token/ERC20/extensions/IERC20Metadata.sol";
import "@openzeppelin/contracts/token/ERC721/extensions/IERC721Metadata.sol";
import { ERC165 } from "@openzeppelin/contracts/utils/introspection/ERC165.sol";
import "./interfaces/IGate.sol";
import "./interfaces/IMinter.sol";
import "./interfaces/ISubscription.sol";
import "./interfaces/ISubscriptionAdapter.sol";
import "./Collectible.sol";
import "./MembershipToken.sol";
import "./TokenStreamer.sol";

abstract contract AbstractGate is IGate, ERC165 {
    MembershipToken public immutable membershipToken;

    constructor(MembershipToken token_) {
        membershipToken = token_;
    }

    function supportsInterface(bytes4 interfaceId)
        public
        view
        virtual
        override
        returns (bool)
    {
        return (
            interfaceId == type(IGate).interfaceId ||
            super.supportsInterface(interfaceId)
        );
    }

    /// @dev Returns true if user has the membership token.
    function isAllowedUser(
        address user
    )
        external
        view
        override
        returns (bool)
    {
        return membershipToken.balanceOf(user) > 0;
    }
}

abstract contract Authority {
    address public immutable authority;
    mapping(bytes32 => bool) internal _requests;

    constructor(address authority_) {
        authority = authority_;
    }
}

abstract contract AbstractMinter is IMinter, Authority, ERC165, Ownable {
    Collectible public immutable collectibleToken;

    constructor(Collectible collectible_) {
        collectibleToken = collectible_;
    }

    function supportsInterface(bytes4 interfaceId)
        public
        view
        virtual
        override
        returns (bool)
    {
        return (
            interfaceId == type(IMinter).interfaceId ||
            super.supportsInterface(interfaceId)
        );
    }

    /// @dev Owner can transfer control over Collectible to the new Adapter contract.
    function transferCollectibleTokenOwnership(address to)
        external
        onlyOwner
    {
        collectibleToken.transferOwnership(to);
    }

    function collectible()
        external
        view
        override
        returns (IERC721Metadata)
    {
        return IERC721Metadata(collectibleToken);
    }

    /// @dev Verifies signature and mints a non-fungible token.
    function mint(
        address user,
        string memory tokenURI,
        uint8 v,
        bytes32 r,
        bytes32 s
    )
        external
        override
    {
        bytes32 dataHash = keccak256(abi.encodePacked(block.chainid, this, "mint", user, tokenURI));
        // Signature is EIP191-compatible (https://eips.ethereum.org/EIPS/eip-191)
        bytes32 messageHash = keccak256(abi.encodePacked("\x19Ethereum Signed Message:\n32", dataHash));
        require(_requests[messageHash] != true, "Adapter: already minted");
        address signer = ecrecover(messageHash, v, r, s);
        require(signer == authority, "Adapter: invalid signature");
        _requests[messageHash] = true;
        collectibleToken.mint(user, tokenURI);
    }
}

abstract contract AbstractSubscriptionAdapter is ISubscriptionAdapter, Authority, ERC165, Ownable {
    TokenStreamer public immutable tokenStreamer;

    constructor(TokenStreamer tokenStreamer_) {
        tokenStreamer = tokenStreamer_;
    }

    function supportsInterface(bytes4 interfaceId)
        public
        view
        virtual
        override
        returns (bool)
    {
        return (
            interfaceId == type(ISubscriptionAdapter).interfaceId ||
            super.supportsInterface(interfaceId)
        );
    }

    /// @dev Owner can transfer control over TokenStreamer to the new Adapter contract.
    function transferSubscriptionContractOwnership(address to)
        external
        onlyOwner
    {
        tokenStreamer.transferOwnership(to);
    }

    /// @dev Closes all token streams before upgrade.
    function beforeTokenStreamerUpgrade()
        external
        onlyOwner
    {
        tokenStreamer.closeAll();
    }

    function subscription()
        external
        view
        override
        returns (ISubscription)
    {
        return ISubscription(tokenStreamer);
    }

    function configureSubscription(
        address recipient,
        uint256 price,
        uint8 v,
        bytes32 r,
        bytes32 s
    )
        external
        override
    {
        bytes32 dataHash = keccak256(abi.encodePacked(block.chainid, this, "configureSubscription", recipient, price));
        // Signature is EIP191-compatible (https://eips.ethereum.org/EIPS/eip-191)
        bytes32 messageHash = keccak256(abi.encodePacked("\x19Ethereum Signed Message:\n32", dataHash));
        require(_requests[messageHash] != true, "Adapter: signature already used");
        address signer = ecrecover(messageHash, v, r, s);
        require(signer == authority, "Adapter: invalid signature");
        _requests[messageHash] = true;
        if (isSubscriptionConfigured(recipient)) {
            tokenStreamer.changeRate(recipient, price);
        } else {
            tokenStreamer.register(recipient, price);
        }
    }

    function isSubscriptionConfigured(address recipient)
        public
        view
        override
        returns (bool)
    {
        return tokenStreamer.recipients(recipient) > 0;
    }

    function subscriptionToken()
        external
        view
        override
        returns (IERC20Metadata)
    {
        return IERC20Metadata(tokenStreamer.token());
    }

    function getSubscriptionPrice(address recipient)
        external
        view
        override
        returns (uint256)
    {
        return tokenStreamer.recipients(recipient);
    }

    function getSubscriptionState(address sender, address recipient)
        external
        view
        override
        returns (uint256, uint256)
    {
        uint256 streamId = tokenStreamer.findStreamByParticipants(sender, recipient);
        if (streamId == 0) {
            return (0, 0);
        }
        uint256 senderBalance = tokenStreamer.balanceOf(streamId, sender);
        uint256 recipientBalance = tokenStreamer.balanceOf(streamId, recipient);
        return (senderBalance, recipientBalance);
    }
}

/**
 * @dev Example of adapter contract.
 */
contract Adapter is AbstractGate, AbstractMinter, AbstractSubscriptionAdapter {
    constructor(
        MembershipToken token_,
        Collectible collectible_,
        TokenStreamer tokenStreamer_,
        address authority_
    )
        Authority(authority_)
        AbstractGate(token_)
        AbstractMinter(collectible_)
        AbstractSubscriptionAdapter(tokenStreamer_)
    {
    }

    function supportsInterface(bytes4 interfaceId)
        public
        view
        override(AbstractGate, AbstractMinter, AbstractSubscriptionAdapter)
        returns (bool)
    {
        return super.supportsInterface(interfaceId);
    }
}
