// SPDX-License-Identifier: GPL-3.0

pragma solidity ^0.8.0;

import "@openzeppelin/contracts/access/Ownable.sol";
import '@openzeppelin/contracts/token/ERC20/ERC20.sol';
import "@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol";
import "@openzeppelin/contracts/utils/Counters.sol";
import "./interfaces/ISubscription.sol";

/**
 * @dev Example of a money streaming contract, based on EIP-1620 proposal (https://eips.ethereum.org/EIPS/eip-1620).
 * This contract is supposed to be controlled by the Adapter contract.
 */
contract TokenStreamer is ISubscription, Ownable {
    using Counters for Counters.Counter;
    using SafeERC20 for ERC20;

    struct Stream {
        address sender;
        address recipient;
        uint256 startTime;
        uint256 stopTime;
    }

    // Set limit on total number of streams to avoid out-of-gas errors in closeAll()
    uint256 private constant STREAM_MAX_COUNT = 100;

    ERC20 public immutable token;
    bool public depositsEnabled = true;

    Counters.Counter private _streamIds;
    mapping(address => uint256) public recipients;
    mapping(uint256 => Stream) private _streams;
    mapping(address => mapping(address => uint256)) private _streamIndex;
    mapping(address => uint256[]) private _streamsByRecipient;

    event RegisterRecipient(
        address indexed recipient,
        uint256 rate
    );
    event ChangeRate(
        address indexed recipient,
        uint256 rate
    );
    event CreateStream(
        uint256 indexed streamId,
        address indexed sender,
        address indexed recipient,
        uint256 deposit
    );
    event ExtendStream(
        uint256 indexed streamId,
        uint256 amount
    );
    event WithdrawFromStream(
        uint256 indexed streamId,
        address addr,
        uint256 amount
    );

    constructor(address tokenAddress) {
        token = ERC20(tokenAddress);
    }

    /**
     * @dev Registers caller as a user who accepts subscriptions.
     * Only Adapter contract can call this method.
     * @param recipient Recipient address.
     * @param rate Stream rate (units per second).
     */
    function register(address recipient, uint256 rate)
        public
        onlyOwner
    {
        require(recipient != address(0));
        require(recipient != address(this));
        require(rate > 0, "TokenStreamer: payment rate must be greater than zero");
        require(recipients[recipient] == 0, "TokenStreamer: recipient already registered");
        recipients[recipient] = rate;
        emit RegisterRecipient(recipient, rate);
    }

    /// @dev Creates a new token stream between msg.sender and recipient.
    function create(address recipient, uint256 deposit)
        public
    {
        require(depositsEnabled, "TokenStreamer: deposits are disabled");
        require(_streamIds.current() < STREAM_MAX_COUNT, "TokenStreamer: too many streams");
        require(recipients[recipient] > 0, "TokenStreamer: unknown recipient");
        require(_streamIndex[msg.sender][recipient] == 0, "TokenStreamer: stream already exists");
        require(msg.sender != recipient, "TokenStreamer: sender and recipient are the same");
        uint256 rate = recipients[recipient];
        require(deposit >= rate, "TokenStreamer: deposit is too small");
        uint256 duration = deposit / rate;
        uint256 exactAmount = duration * rate;
        uint256 startTime = block.timestamp;
        uint256 stopTime = startTime + duration;
        Stream memory stream = Stream(msg.sender, recipient, startTime, stopTime);
        _streamIds.increment();
        uint256 streamId = _streamIds.current();
        _streams[streamId] = stream;
        _streamIndex[msg.sender][recipient] = streamId;
        _streamsByRecipient[recipient].push(streamId);
        token.safeTransferFrom(msg.sender, address(this), exactAmount);
        emit CreateStream(streamId, msg.sender, recipient, exactAmount);
        emit UpdateSubscription(msg.sender, recipient, stream.stopTime);
    }

    function getStream(uint256 streamId)
        public
        view
        returns (address sender, address recipient, uint256 startTime, uint256 stopTime)
    {
        Stream memory stream = _streams[streamId];
        require(stream.recipient != address(0), "TokenStreamer: stream does not exist");
        return (
            stream.sender,
            stream.recipient,
            stream.startTime,
            stream.stopTime
        );
    }

    function balanceOf(uint256 streamId, address addr)
        public
        view
        returns (uint256 balance)
    {
        Stream memory stream = _streams[streamId];
        require(stream.recipient != address(0), "TokenStreamer: stream does not exist");
        uint256 rate = recipients[stream.recipient];
        if (addr == stream.sender) {
            if (block.timestamp >= stream.stopTime) {
                return 0;
            } else {
                return (stream.stopTime - block.timestamp) * rate;
            }
        } else if (addr == stream.recipient) {
            if (block.timestamp >= stream.stopTime) {
                return (stream.stopTime - stream.startTime) * rate;
            } else {
                return (block.timestamp - stream.startTime) * rate;
            }
        } else {
            revert("TokenStreamer: invalid address");
        }
    }

    /// @dev Adds funds to the token stream.
    function extend(uint256 streamId, uint256 amount)
        public
    {
        require(depositsEnabled, "TokenStreamer: deposits are disabled");
        Stream storage stream = _streams[streamId];
        require(stream.recipient != address(0), "TokenStreamer: stream does not exist");
        require(msg.sender == stream.sender, "TokenStreamer: caller is not the sender");
        uint256 rate = recipients[stream.recipient];
        require(amount >= rate, "TokenStreamer: deposit is too small");
        if (stream.stopTime < block.timestamp) {
            // Shift the window to keep recipient balance intact
            uint256 window = stream.stopTime - stream.startTime;
            stream.stopTime = block.timestamp;
            stream.startTime = stream.stopTime - window;
        }
        // Extend
        uint256 duration = amount / rate;
        uint256 exactAmount = duration * rate;
        stream.stopTime = stream.stopTime + duration;
        token.safeTransferFrom(msg.sender, address(this), exactAmount);
        emit ExtendStream(streamId, exactAmount);
        emit UpdateSubscription(stream.sender, stream.recipient, stream.stopTime);
    }

    /// @dev Sends all or a fraction of the available funds to the caller.
    function withdraw(uint256 streamId, uint256 amount)
        public
    {
        Stream storage stream = _streams[streamId];
        require(stream.recipient != address(0), "TokenStreamer: stream does not exist");
        uint256 rate = recipients[stream.recipient];
        require(amount >= rate, "TokenStreamer: withdrawal amount is too small");
        require(amount % rate == 0, "TokenStreamer: withdrawal amount is not a multiple of rate");
        uint256 balance = balanceOf(streamId, msg.sender);
        require(amount <= balance, "TokenStreamer: amount exceeds available balance");
        if (msg.sender == stream.sender) {
            stream.stopTime = stream.stopTime - amount / rate;
        } else if (msg.sender == stream.recipient) {
            stream.startTime = stream.startTime + amount / rate;
        } else {
            revert();
        }
        token.safeTransfer(msg.sender, amount);
        emit WithdrawFromStream(streamId, msg.sender, amount);
        emit UpdateSubscription(stream.sender, stream.recipient, stream.stopTime);
    }

    function _close(uint256 streamId)
        private
    {
        Stream storage stream = _streams[streamId];
        uint256 rate = recipients[stream.recipient];
        uint256 senderBalance = balanceOf(streamId, stream.sender);
        stream.stopTime = stream.stopTime - senderBalance / rate;
        uint256 recipientBalance = balanceOf(streamId, stream.recipient);
        stream.startTime = stream.startTime + recipientBalance / rate;
        assert(stream.startTime == stream.stopTime); // stream is empty
        if (senderBalance > 0) {
            token.safeTransfer(stream.sender, senderBalance);
            emit WithdrawFromStream(streamId, stream.sender, senderBalance);
        }
        if (recipientBalance > 0) {
            token.safeTransfer(stream.recipient, recipientBalance);
            emit WithdrawFromStream(streamId, stream.recipient, recipientBalance);
        }
        emit UpdateSubscription(stream.sender, stream.recipient, stream.stopTime);
    }

    /// @dev Sends all remaining funds to participants.
    function close(uint256 streamId)
        public
    {
        Stream memory stream = _streams[streamId];
        require(stream.recipient != address(0), "TokenStreamer: stream does not exist");
        require(
            msg.sender == stream.sender || msg.sender == stream.recipient,
            "TokenStreamer: caller is not a participant"
        );
        _close(streamId);
    }

    /// @dev Finds stream by sender and recipient address and returns its ID
    function findStreamByParticipants(address sender, address recipient)
        public
        view
        returns (uint256)
    {
        return _streamIndex[sender][recipient];
    }

    function send(address recipient, uint256 amount)
        override
        external
    {
        uint256 streamId = _streamIndex[msg.sender][recipient];
        if (streamId == 0) {
            create(recipient, amount);
        } else {
            extend(streamId, amount);
        }
    }

    function cancel(address recipient)
        override
        external
    {
        uint256 streamId = findStreamByParticipants(msg.sender, recipient);
        close(streamId);
    }

    function withdrawReceived(address sender)
        override
        external
    {
        uint256 streamId = findStreamByParticipants(sender, msg.sender);
        uint256 balance = balanceOf(streamId, msg.sender);
        withdraw(streamId, balance);
    }

    function withdrawReceivedAll()
        external
        override
    {
        uint256[] memory streams = _streamsByRecipient[msg.sender];
        for (uint256 index = 0; index < streams.length; index++) {
            uint256 streamId = streams[index];
            uint256 balance = balanceOf(streamId, msg.sender);
            withdraw(streamId, balance);
        }
    }

    /// @dev Changes recipient's default stream rate.
    function changeRate(address recipient, uint256 rate)
        public
        onlyOwner
    {
        require(rate > 0, "TokenStreamer: payment rate must be greater than zero");
        require(recipients[recipient] > 0, "TokenStreamer: unknown recipient");
        // Close all streams before changing rate
        uint256[] memory streams = _streamsByRecipient[recipient];
        for (uint256 index = 0; index < streams.length; index++) {
            uint256 streamId = streams[index];
            _close(streamId);
        }
        recipients[recipient] = rate;
        emit ChangeRate(recipient, rate);
    }

    /// @dev Disables deposits and closes all streams.
    function closeAll()
        public
        onlyOwner
    {
        depositsEnabled = false;
        for (uint256 index = 0; index < _streamIds.current(); index++) {
            _close(index + 1);
        }
    }
}
