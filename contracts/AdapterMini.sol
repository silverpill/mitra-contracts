// SPDX-License-Identifier: GPL-3.0

pragma solidity ^0.8.0;

import { TokenStreamer } from "./TokenStreamer.sol";
import { AbstractSubscriptionAdapter, Authority } from "./Adapter.sol";

/**
 * @dev Adapter contract without token gate and minter functionality.
 */
contract AdapterMini is AbstractSubscriptionAdapter {

    constructor(
        TokenStreamer tokenStreamer_,
        address authority_
    )
        Authority(authority_)
        AbstractSubscriptionAdapter(tokenStreamer_)
    {
    }
}
