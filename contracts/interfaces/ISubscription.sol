// SPDX-License-Identifier: GPL-3.0

pragma solidity ^0.8.0;

/// @dev Interface for subscription contract.
interface ISubscription {
    event UpdateSubscription(
        address indexed sender,
        address indexed recipient,
        uint256 expires_at
    );

    /// @dev Create or update token stream.
    function send(address recipient, uint256 amount) external;

    /// @dev Cancel subscription.
    function cancel(address recipient) external;

    /// @dev Withdraw tokens received from a given sender.
    function withdrawReceived(address sender) external;

    /// @dev Withdraw received tokens from all streams.
    function withdrawReceivedAll() external;
}
