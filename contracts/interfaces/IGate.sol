// SPDX-License-Identifier: GPL-3.0

pragma solidity ^0.8.0;

/// @dev Provides a standard interface of token gate for Mitra instance.
interface IGate {
    /// @dev Returns true if user is allowed to sign up.
    function isAllowedUser(address user) external view returns (bool);
}
