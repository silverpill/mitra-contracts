// SPDX-License-Identifier: GPL-3.0

pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC721/extensions/IERC721Metadata.sol";

/// @dev Provides a standard interface of NFT minter for Mitra instance.
interface IMinter {
    /// @dev Returns address of the collectible token contract.
    function collectible() external view returns (IERC721Metadata);

    /// @dev Verifies signature and mints a non-fungible token.
    function mint(address user, string memory tokenURI, uint8 v, bytes32 r, bytes32 s) external;
}
