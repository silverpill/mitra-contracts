// SPDX-License-Identifier: GPL-3.0

pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC20/extensions/IERC20Metadata.sol";
import "./ISubscription.sol";

/// @dev Provides a standard interface of subscription manager for Mitra instance.
interface ISubscriptionAdapter {
    /// @dev Returns address of subscription contract.
    function subscription() external view returns (ISubscription);

    /// @dev Registers user as subscription payment recipient.
    function configureSubscription(address recipient, uint256 price, uint8 v, bytes32 r, bytes32 s) external;

    /// @dev Returns true if user accepts subscription payments.
    function isSubscriptionConfigured(address recipient) external view returns (bool);

    /// @dev Returns address of subscription payment token.
    function subscriptionToken() external view returns (IERC20Metadata);

    /// @dev Returns subscription price (tokens per second).
    function getSubscriptionPrice(address recipient) external view returns (uint256);

    /// @dev Returns current state of the subscription payment.
    function getSubscriptionState(address sender, address recipient) external view returns (uint256, uint256);
}
