import { expect } from "chai"
import { ethers, waffle } from "hardhat"
import { Contract } from "ethers"
import { AddressZero } from "@ethersproject/constants"

describe("Membership token", () => {
  const { provider } = waffle
  const [deployer, user, someone] = provider.getWallets()

  let token: Contract

  beforeEach(async () => {
    const MembershipToken = await ethers.getContractFactory("MembershipToken")
    token = await MembershipToken.deploy("Membership token", "ABC")
    await token.deployed()
  })

  it("Should deploy token contract", async () => {
    expect(await token.name()).to.equal("Membership token")
    expect(await token.symbol()).to.equal("ABC")
    expect(await token.owner()).to.equal(deployer.address)
  })

  it("Should transfer ownership", async () => {
    await expect(token.transferOwnership(someone.address))
      .to.emit(token, "OwnershipTransferred")
      .withArgs(deployer.address, someone.address)
  })

  it("Should mint token", async () => {
    await expect(token.mint(user.address))
      .to.emit(token, "Transfer")
      .withArgs(AddressZero, user.address, 1)
  })

  it("Should not mint token if caller is not the owner", async () => {
    await expect(token.connect(user).mint(user.address))
      .to.be.revertedWith("Ownable: caller is not the owner")
  })

  it("Should not mint token if recipient already has one", async () => {
    await token.mint(user.address)
    await expect(token.mint(user.address))
      .to.be.revertedWith("MembershipToken: token already minted")
  })

  it("Should not transfer token", async () => {
    await token.mint(user.address)
    await expect(token.connect(user).transferFrom(user.address, someone.address, 1))
      .to.be.revertedWith("MembershipToken: transfers are not allowed")

    const safeTranferFrom = token.connect(user)
      .functions["safeTransferFrom(address,address,uint256)"]
    await expect(safeTranferFrom(user.address, someone.address, 1))
      .to.be.revertedWith("MembershipToken: transfers are not allowed")
  })
})
