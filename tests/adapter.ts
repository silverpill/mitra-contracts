import { expect } from "chai"
import { artifacts, ethers, waffle } from "hardhat"
import { Contract } from "ethers"
import { Interface } from "@ethersproject/abi"
import { BigNumber } from "@ethersproject/bignumber"
import { arrayify, hexlify, splitSignature, Signature } from "@ethersproject/bytes"
import { AddressZero, WeiPerEther, Zero } from "@ethersproject/constants"
import { keccak256 } from "@ethersproject/solidity"

async function getInterfaceId(name: string): Promise<string> {
  const artifact = await artifacts.readArtifact(name)
  const interface_ = new Interface(artifact.abi)
  let result = new Uint8Array([0, 0, 0, 0])
  for (const func of Object.values(interface_.functions)) {
    const sig = arrayify(interface_.getSighash(func))
    result = Uint8Array.from(result, (value, index) => value ^ sig[index])
  }
  return hexlify(result)
}

describe("Adapter", function () {
  const { provider } = waffle
  const [deployer, authority, user, someone] = provider.getWallets()

  const tokenUri = "ipfs://bafy"
  let membershipToken: Contract
  let collectible: Contract
  let paymentToken: Contract
  let tokenStreamer: Contract
  let adapter: Contract

  async function getMintSignature(recipient: string, tokenUri: string): Promise<Signature> {
    const chainId = (await provider.getNetwork()).chainId
    const message = keccak256(
      ["uint256", "address", "string", "address", "string"],
      [chainId, adapter.address, "mint", recipient, tokenUri],
    )
    const rawSignature = await authority.signMessage(arrayify(message))
    const signature = splitSignature(rawSignature)
    return signature
  }

  async function getSubscriptionSignature(recipient: string, price: BigNumber): Promise<Signature> {
    const chainId = (await provider.getNetwork()).chainId
    const message = keccak256(
      ["uint256", "address", "string", "address", "uint256"],
      [chainId, adapter.address, "configureSubscription", recipient, price],
    )
    const rawSignature = await authority.signMessage(arrayify(message))
    const signature = splitSignature(rawSignature)
    return signature
  }

  beforeEach(async () => {
    const MembershipToken = await ethers.getContractFactory("MembershipToken")
    membershipToken = await MembershipToken.deploy("Membership token", "ABC")
    const Collectible = await ethers.getContractFactory("Collectible")
    collectible = await Collectible.deploy("Collectible", "NFT")
    const PaymentToken = await ethers.getContractFactory("PaymentToken")
    paymentToken = await PaymentToken.deploy("Payment token", "ABC", WeiPerEther.mul(100))
    const TokenStreamer = await ethers.getContractFactory("TokenStreamer")
    tokenStreamer = await TokenStreamer.deploy(paymentToken.address)
    const Adapter = await ethers.getContractFactory("Adapter")
    adapter = await Adapter.deploy(
      membershipToken.address,
      collectible.address,
      tokenStreamer.address,
      authority.address,
    )
    collectible.transferOwnership(adapter.address)
    tokenStreamer.transferOwnership(adapter.address)
    await adapter.deployed()
  })

  it("Should deploy adapter contract", async () => {
    // ERC165 interface ID
    expect(await adapter.supportsInterface("0x01ffc9a7")).to.equal(true)
    // Other interfaces
    const IGateInterfaceId = await getInterfaceId("IGate")
    expect(await adapter.supportsInterface(IGateInterfaceId)).to.equal(true)
    const IMinterInterfaceId = await getInterfaceId("IMinter")
    expect(await adapter.supportsInterface(IMinterInterfaceId)).to.equal(true)
    const ISubscriptionAdapterInterfaceId = await getInterfaceId("ISubscriptionAdapter")
    expect(await adapter.supportsInterface(ISubscriptionAdapterInterfaceId)).to.equal(true)

    expect(await adapter.membershipToken()).to.equal(membershipToken.address)
    expect(await adapter.collectibleToken()).to.equal(collectible.address)
    expect(await adapter.collectible()).to.equal(collectible.address)
    expect(await adapter.tokenStreamer()).to.equal(tokenStreamer.address)
    expect(await adapter.subscription()).to.equal(tokenStreamer.address)
    expect(await adapter.subscriptionToken()).to.equal(paymentToken.address)
    expect(await adapter.authority()).to.equal(authority.address)
    expect(await collectible.owner()).to.equal(adapter.address)
  })

  it("Should transfer ownership", async () => {
    await expect(adapter.transferOwnership(authority.address))
      .to.emit(adapter, "OwnershipTransferred")
      .withArgs(deployer.address, authority.address)
  })

  it("Should transfer ownership of collectible token contract", async () => {
    await expect(adapter.transferCollectibleTokenOwnership(authority.address))
      .to.emit(collectible, "OwnershipTransferred")
      .withArgs(adapter.address, authority.address)
  })

  it("Should not transfer ownership of collectible token if caller is not the owner", async () => {
    await expect(adapter.connect(user).transferCollectibleTokenOwnership(user.address))
      .to.be.revertedWith("Ownable: caller is not the owner")
  })

  it("Should transfer ownership of subscription contract", async () => {
    await expect(adapter.transferSubscriptionContractOwnership(authority.address))
      .to.emit(tokenStreamer, "OwnershipTransferred")
      .withArgs(adapter.address, authority.address)
  })

  it("Should not transfer ownership of subscription contract if caller is not the owner", async () => {
    await expect(adapter.connect(user).transferSubscriptionContractOwnership(user.address))
      .to.be.revertedWith("Ownable: caller is not the owner")
  })

  it("Should close all token streams before upgrade", async () => {
    await adapter.beforeTokenStreamerUpgrade()
    expect(await tokenStreamer.depositsEnabled()).to.equal(false)
  })

  it("Should verify user's membership status", async () => {
    expect(await adapter.isAllowedUser(user.address)).to.equal(false)
    await membershipToken.mint(user.address)
    expect(await adapter.isAllowedUser(user.address)).to.equal(true)
  })

  it("Should mint non-fungible token", async () => {
    const signature = await getMintSignature(user.address, tokenUri)
    await expect(adapter.connect(user).mint(
      user.address, tokenUri,
      signature.v, signature.r, signature.s,
    ))
      .to.emit(collectible, "Transfer")
      .withArgs(AddressZero, user.address, 1)

    expect(await collectible.ownerOf(1)).to.equal(user.address)
    expect(await collectible.tokenURI(1)).to.equal(tokenUri)
  })

  it("Should fail to mint token if signature is created for another user", async () => {
    const signature = await getMintSignature(user.address, tokenUri)
    await expect(adapter.connect(someone).mint(
      someone.address, tokenUri,
      signature.v, signature.r, signature.s,
    ))
      .to.be.revertedWith("Adapter: invalid signature")
  })

  it("Should fail to mint token if signature is created for another URI", async () => {
    const signature = await getMintSignature(user.address, "fake URI")
    await expect(adapter.connect(user).mint(
      user.address, tokenUri,
      signature.v, signature.r, signature.s,
    ))
      .to.be.revertedWith("Adapter: invalid signature")
  })

  it("Should fail to mint token if it is already minted", async () => {
    const signature = await getMintSignature(user.address, tokenUri)
    await adapter.connect(user).mint(
      user.address, tokenUri,
      signature.v, signature.r, signature.s,
    )
    await expect(adapter.connect(user).mint(
      user.address, tokenUri,
      signature.v, signature.r, signature.s,
    ))
      .to.be.revertedWith("Adapter: already minted")
  })

  it("Should register user as subscription payment recipient", async () => {
    const subscriptionPrice = WeiPerEther
    const signature = await getSubscriptionSignature(user.address, subscriptionPrice)
    await expect(adapter.connect(user).configureSubscription(
      user.address,
      subscriptionPrice,
      signature.v, signature.r, signature.s,
    ))
      .to.emit(tokenStreamer, "RegisterRecipient")
      .withArgs(user.address, subscriptionPrice)
    expect(await adapter.isSubscriptionConfigured(user.address)).to.equal(true)

    expect(await adapter.getSubscriptionPrice(user.address))
      .to.equal(subscriptionPrice)
  })

  it("Should update user's subscription price", async () => {
    let subscriptionPrice = WeiPerEther
    let signature = await getSubscriptionSignature(user.address, subscriptionPrice)
    await adapter.connect(user).configureSubscription(
      user.address,
      subscriptionPrice,
      signature.v, signature.r, signature.s,
    )
    subscriptionPrice = WeiPerEther.mul(2)
    signature = await getSubscriptionSignature(user.address, subscriptionPrice)
    await expect(adapter.connect(user).configureSubscription(
      user.address,
      subscriptionPrice,
      signature.v, signature.r, signature.s,
    ))
      .to.emit(tokenStreamer, "ChangeRate")
      .withArgs(user.address, subscriptionPrice)
    expect(await adapter.getSubscriptionPrice(user.address))
      .to.equal(subscriptionPrice)
  })

  it("Should not register subscription payment recipient if signature is created for another user", async () => {
    const subscriptionPrice = WeiPerEther
    const signature = await getSubscriptionSignature(user.address, subscriptionPrice)
    await expect(adapter.connect(someone).configureSubscription(
      someone.address,
      subscriptionPrice,
      signature.v, signature.r, signature.s,
    ))
      .to.be.revertedWith("Adapter: invalid signature")
  })

  it("Should fail to register subscription payment recipient if signature is re-used", async () => {
    const subscriptionPrice = WeiPerEther
    const signature = await getSubscriptionSignature(user.address, subscriptionPrice)
    await adapter.connect(user).configureSubscription(
      user.address,
      subscriptionPrice,
      signature.v, signature.r, signature.s,
    )
    await expect(adapter.connect(user).configureSubscription(
      user.address,
      subscriptionPrice,
      signature.v, signature.r, signature.s,
    ))
      .to.be.revertedWith("Adapter: signature already used")
  })

  it("Should check subscription status", async () => {
    const deposit = WeiPerEther.mul(10)
    await paymentToken.transfer(someone.address, deposit)
    await paymentToken.connect(someone).increaseAllowance(tokenStreamer.address, deposit)
    const subscriptionPrice = WeiPerEther
    const signature = await getSubscriptionSignature(user.address, subscriptionPrice)
    await adapter.configureSubscription(
      user.address,
      subscriptionPrice,
      signature.v, signature.r, signature.s,
    )
    expect(await adapter.getSubscriptionState(someone.address, user.address))
      .to.deep.equal([Zero, Zero])
    await tokenStreamer.connect(someone).create(user.address, deposit)
    expect(await adapter.getSubscriptionState(someone.address, user.address))
      .to.deep.equal([deposit, Zero])
  })
})
