import { expect } from "chai"
import { ethers, waffle } from "hardhat"
import { BigNumber, Contract } from "ethers"
import { AddressZero } from "@ethersproject/constants"
import { ContractTransaction } from "@ethersproject/contracts"

describe("Collectible", function () {
  const { provider } = waffle
  const [deployer, minter, user, buyer] = provider.getWallets()

  const tokenUri = "ipfs://bafy"
  let collectible: Contract

  async function getTokenId(mintTx: ContractTransaction): Promise<BigNumber> {
    const receipt = await mintTx.wait()
    const tokenId = (receipt.events as any)[0].args.tokenId
    return tokenId
  }

  beforeEach(async () => {
    const Collectible = await ethers.getContractFactory("Collectible")
    collectible = await Collectible.deploy("Collectible", "NFT")
    await collectible.deployed()
  })

  it("Should deploy token contract", async () => {
    expect(await collectible.name()).to.equal("Collectible")
    expect(await collectible.symbol()).to.equal("NFT")
    expect(await collectible.owner()).to.equal(deployer.address)
  })

  it("Should transfer ownership", async () => {
    await expect(collectible.transferOwnership(minter.address))
      .to.emit(collectible, "OwnershipTransferred")
      .withArgs(deployer.address, minter.address)
  })

  it("Should mint token", async () => {
    await expect(collectible.mint(user.address, tokenUri))
      .to.emit(collectible, "Transfer")
      .withArgs(AddressZero, user.address, 1)

    expect(await collectible.ownerOf(1)).to.equal(user.address)
    expect(await collectible.tokenURI(1)).to.equal(tokenUri)
  })

  it("Should increment token counter", async () => {
    await expect(collectible.mint(user.address, "tokenUri1"))
      .to.emit(collectible, "Transfer")
      .withArgs(AddressZero, user.address, 1)
    await expect(collectible.mint(user.address, "tokenUri2"))
      .to.emit(collectible, "Transfer")
      .withArgs(AddressZero, user.address, 2)
  })

  it("Should not mint token if caller is not the owner", async () => {
    await expect(collectible.connect(user).mint(user.address, tokenUri))
      .to.be.revertedWith("Ownable: caller is not the owner")
  })

  it("Should transfer token", async () => {
    const mintTx = await collectible.mint(user.address, tokenUri)
    const tokenId = await getTokenId(mintTx)
    await expect(collectible.connect(user).transferFrom(
      user.address, buyer.address, tokenId,
    ))
      .to.emit(collectible, "Transfer")
  })

  it("Should not transfer token that is not own", async () => {
    const mintTx = await collectible.mint(user.address, tokenUri)
    const tokenId = await getTokenId(mintTx)
    await expect(collectible.connect(buyer).transferFrom(
      user.address, buyer.address, tokenId,
    ))
      .to.be.revertedWith("ERC721: transfer caller is not owner nor approved")
  })
})
