import { expect } from "chai"
import { ethers, waffle } from "hardhat"
import { BigNumber, Contract } from "ethers"
import { AddressZero, WeiPerEther, Zero } from "@ethersproject/constants"
import { ContractTransaction } from "@ethersproject/contracts"

describe("Token streamer", () => {
  const { provider } = waffle
  const [
    deployer,
    sender,
    anotherSender,
    recipient,
    someone,
  ] = provider.getWallets()

  const tokenSupply = WeiPerEther.mul(1000)
  const paymentRate = WeiPerEther
  const initialSenderBalance = tokenSupply.div(10)
  let token: Contract
  let streamer: Contract

  beforeEach(async () => {
    const PaymentToken = await ethers.getContractFactory("PaymentToken")
    token = await PaymentToken.deploy("Payment token", "ABC", tokenSupply)
    await token.transfer(sender.address, initialSenderBalance)
    await token.transfer(anotherSender.address, initialSenderBalance)
    const TokenStreamer = await ethers.getContractFactory("TokenStreamer")
    streamer = await TokenStreamer.deploy(token.address)
    await streamer.deployed()
  })

  it("Should deploy contract", async () => {
    expect(await streamer.owner()).to.equal(deployer.address)
    expect(await streamer.token()).to.equal(token.address)
    expect(await streamer.depositsEnabled()).to.equal(true)
  })

  describe("Registration", () => {
    it("Should register recipient", async () => {
      expect(await streamer.recipients(recipient.address)).to.equal(Zero)
      await expect(streamer.register(recipient.address, paymentRate))
        .to.emit(streamer, "RegisterRecipient")
        .withArgs(recipient.address, paymentRate)
      expect(await streamer.recipients(recipient.address)).to.equal(paymentRate)
    })

    it("Should not register recipient if caller is not the owner", async () => {
      await expect(streamer.connect(recipient).register(recipient.address, paymentRate))
        .to.be.revertedWith("Ownable: caller is not the owner")
    })

    it("Should not register recipient who is already registered", async () => {
      await streamer.register(recipient.address, paymentRate)
      await expect(streamer.register(recipient.address, paymentRate))
        .to.be.revertedWith("TokenStreamer: recipient already registered")
    })

    it("Should not accept zero address as recipient address", async () => {
      await expect(streamer.register(AddressZero, paymentRate)).to.be.reverted
    })

    it("Should not accept current contract address as recipient address", async () => {
      await expect(streamer.register(streamer.address, paymentRate)).to.be.reverted
    })

    it("Should revert if payment rate is zero", async () => {
      await expect(streamer.register(recipient.address, 0))
        .to.be.revertedWith("TokenStreamer: payment rate must be greater than zero")
    })
  })

  describe("Creating stream", () => {
    const deposit = WeiPerEther.mul(10)

    it("Should create token stream", async () => {
      await streamer.register(recipient.address, paymentRate)
      await token.connect(sender).increaseAllowance(streamer.address, deposit)
      const streamCreated = await streamer.connect(sender).create(recipient.address, deposit)
      const currentTime = (await provider.getBlock("latest")).timestamp
      const expectedStreamId = 1
      const expectedDuration = deposit.div(paymentRate).toNumber()
      await expect(streamCreated)
        .to.emit(streamer, "CreateStream")
        .withArgs(
          expectedStreamId,
          sender.address,
          recipient.address,
          deposit,
        )
        .to.emit(streamer, "UpdateSubscription")
        .withArgs(
          sender.address,
          recipient.address,
          currentTime + expectedDuration,
        )

      const stream = await streamer.getStream(expectedStreamId)
      expect(stream.sender).to.equal(sender.address)
      expect(stream.recipient).to.equal(recipient.address)
      expect(stream.startTime).to.equal(currentTime)
      expect(stream.stopTime).to.equal(currentTime + expectedDuration)
      expect(await token.balanceOf(sender.address))
        .to.equal(initialSenderBalance.sub(deposit))
      expect(await token.balanceOf(streamer.address)).to.equal(deposit)
    })

    it("Should create token stream if deposit amount is not a multiple of rate", async () => {
      const deposit = WeiPerEther.mul(21).div(2)
      const expectedAmount = WeiPerEther.mul(10)
      await streamer.register(recipient.address, paymentRate)
      await token.connect(sender).increaseAllowance(streamer.address, deposit)
      const expectedStreamId = 1
      await expect(streamer.connect(sender).create(recipient.address, deposit))
        .to.emit(streamer, "CreateStream")
        .withArgs(
          expectedStreamId,
          sender.address,
          recipient.address,
          expectedAmount,
        )
      expect(await token.balanceOf(sender.address))
        .to.equal(initialSenderBalance.sub(expectedAmount))
      expect(await token.balanceOf(streamer.address))
        .to.equal(expectedAmount)
    })

    it("Should not create stream if recipient is not registered", async () => {
      await token.connect(sender).increaseAllowance(streamer.address, deposit)
      await expect(streamer.connect(sender).create(recipient.address, deposit))
        .to.be.revertedWith("TokenStreamer: unknown recipient")
    })

    it("Should not create stream if it already exists", async () => {
      await streamer.register(recipient.address, paymentRate)
      await token.connect(sender).increaseAllowance(streamer.address, deposit)
      await streamer.connect(sender).create(recipient.address, deposit)
      await expect(streamer.connect(sender).create(recipient.address, deposit))
        .to.be.revertedWith("TokenStreamer: stream already exists")
    })

    it("Should fail if sender and recipient are the same address", async () => {
      await streamer.register(recipient.address, paymentRate)
      await expect(streamer.connect(recipient).create(recipient.address, deposit))
        .to.be.revertedWith("TokenStreamer: sender and recipient are the same")
    })

    it("Should fail if deposit amount is zero", async () => {
      await streamer.register(recipient.address, paymentRate)
      await token.connect(sender).increaseAllowance(streamer.address, deposit)
      await expect(streamer.connect(sender).create(recipient.address, 0))
        .to.be.revertedWith("TokenStreamer: deposit is too small")
    })

    it("Should fail if deposit amount is too small", async () => {
      await streamer.register(recipient.address, paymentRate)
      await token.connect(sender).increaseAllowance(streamer.address, deposit)
      await expect(streamer.connect(sender).create(recipient.address, paymentRate.div(10)))
        .to.be.revertedWith("TokenStreamer: deposit is too small")
    })

    it("Should fail if deposit amount is not approved", async () => {
      await streamer.register(recipient.address, paymentRate)
      await expect(streamer.connect(sender).create(recipient.address, deposit))
        .to.be.revertedWith("ERC20: transfer amount exceeds allowance")
    })
  })

  async function getStreamId(createTx: ContractTransaction): Promise<BigNumber> {
    const receipt = await createTx.wait()
    const streamId = (receipt.events as any)[2].args.streamId
    return streamId
  }

  it("Should find stream by participant addresses", async () => {
    const deposit = WeiPerEther.mul(10)
    await streamer.register(recipient.address, paymentRate)
    await token.connect(sender).increaseAllowance(streamer.address, deposit)
    const streamCreated = await streamer.connect(sender).create(recipient.address, deposit)
    const streamId = await getStreamId(streamCreated)

    expect(await streamer.findStreamByParticipants(sender.address, recipient.address)).to.equal(streamId)
    expect(await streamer.findStreamByParticipants(recipient.address, sender.address)).to.equal(0)
  })

  it("Should calculate balances", async () => {
    const deposit = WeiPerEther.mul(10)
    await streamer.register(recipient.address, paymentRate)
    await token.connect(sender).increaseAllowance(streamer.address, deposit)

    await expect(streamer.balanceOf(1, sender.address)).to.be.reverted
    await expect(streamer.balanceOf(1, recipient.address)).to.be.reverted

    // Create stream
    const streamCreated = await streamer.connect(sender).create(recipient.address, deposit)
    const streamId = await getStreamId(streamCreated)
    const currentTime = (await provider.getBlock("latest")).timestamp
    const duration = deposit.div(paymentRate).toNumber()

    // T = 0
    expect(await streamer.balanceOf(streamId, sender.address)).to.equal(deposit)
    expect(await streamer.balanceOf(streamId, recipient.address)).to.equal(0)
    await expect(streamer.balanceOf(streamId, someone.address))
      .to.be.revertedWith("TokenStreamer: invalid address")

    // T = D / 10
    await provider.send("evm_setNextBlockTimestamp", [currentTime + duration / 10])
    await provider.send("evm_mine", [])
    expect(await streamer.balanceOf(streamId, sender.address)).to.equal(deposit.div(10).mul(9))
    expect(await streamer.balanceOf(streamId, recipient.address)).to.equal(deposit.div(10))
    // T = D / 2
    await provider.send("evm_setNextBlockTimestamp", [currentTime + duration / 2])
    await provider.send("evm_mine", [])
    expect(await streamer.balanceOf(streamId, sender.address)).to.equal(deposit.div(2))
    expect(await streamer.balanceOf(streamId, recipient.address)).to.equal(deposit.div(2))
    // T = D
    await provider.send("evm_setNextBlockTimestamp", [currentTime + duration])
    await provider.send("evm_mine", [])
    expect(await streamer.balanceOf(streamId, sender.address)).to.equal(0)
    expect(await streamer.balanceOf(streamId, recipient.address)).to.equal(deposit)
    // T = D * 2
    await provider.send("evm_setNextBlockTimestamp", [currentTime + duration * 2])
    await provider.send("evm_mine", [])
    expect(await streamer.balanceOf(streamId, sender.address)).to.equal(0)
    expect(await streamer.balanceOf(streamId, recipient.address)).to.equal(deposit)
  })

  describe("Adding funds to the stream", () => {
    const initialDeposit = WeiPerEther.mul(10)
    const amount = WeiPerEther.mul(3)
    let streamId: BigNumber
    let streamStartTime: number
    let streamDuration: number

    beforeEach(async () => {
      await streamer.register(recipient.address, paymentRate)
      await token.connect(sender).increaseAllowance(streamer.address, initialDeposit.add(amount))
      const streamCreated = await streamer.connect(sender).create(recipient.address, initialDeposit)
      streamId = await getStreamId(streamCreated)
      const stream = await streamer.getStream(streamId)
      streamStartTime = stream.startTime.toNumber()
      streamDuration = stream.stopTime - stream.startTime
    })

    it("Should add tokens to stream", async () => {
      const timestamp = streamStartTime + streamDuration / 2 - 1
      await provider.send("evm_setNextBlockTimestamp", [timestamp])
      await provider.send("evm_mine", [])
      const expectedStopTime = streamStartTime + streamDuration + amount.div(paymentRate).toNumber()
      await expect(streamer.connect(sender).extend(streamId, amount))
        .to.emit(streamer, "ExtendStream")
        .withArgs(streamId, amount)
        .to.emit(streamer, "UpdateSubscription")
        .withArgs(sender.address, recipient.address, expectedStopTime)

      expect(await streamer.balanceOf(streamId, sender.address))
        .to.equal(initialDeposit.div(2).add(amount))
      expect(await streamer.balanceOf(streamId, recipient.address))
        .to.equal(initialDeposit.div(2))
      expect(await token.balanceOf(sender.address))
        .to.equal(initialSenderBalance.sub(initialDeposit).sub(amount))
      expect(await token.balanceOf(streamer.address))
        .to.equal(initialDeposit.add(amount))
    })

    it("Should add tokens to stream if amount is not a multiple of rate", async () => {
      const amount = WeiPerEther.mul(5).div(2)
      const expectedAmount = WeiPerEther.mul(2)
      await expect(streamer.connect(sender).extend(streamId, amount))
        .to.emit(streamer, "ExtendStream")
        .withArgs(streamId, expectedAmount)

      expect(await token.balanceOf(sender.address))
        .to.equal(initialSenderBalance.sub(initialDeposit).sub(expectedAmount))
      expect(await token.balanceOf(streamer.address))
        .to.equal(initialDeposit.add(expectedAmount))
    })

    it("Should re-open closed stream", async () => {
      const timestamp = streamStartTime + streamDuration * 2
      await provider.send("evm_setNextBlockTimestamp", [timestamp])
      await provider.send("evm_mine", [])
      expect(await streamer.balanceOf(streamId, sender.address)).to.equal(0)
      const currentTime = (await provider.getBlock("latest")).timestamp
      const expectedStopTime = currentTime + amount.div(paymentRate).toNumber() + 1
      await expect(streamer.connect(sender).extend(streamId, amount))
        .to.emit(streamer, "ExtendStream")
        .withArgs(streamId, amount)
        .to.emit(streamer, "UpdateSubscription")
        .withArgs(sender.address, recipient.address, expectedStopTime)

      expect(await streamer.balanceOf(streamId, sender.address))
        .to.equal(amount)
      expect(await streamer.balanceOf(streamId, recipient.address))
        .to.equal(initialDeposit)
      expect(await token.balanceOf(streamer.address))
        .to.equal(initialDeposit.add(amount))
    })

    it("Should fail if stream does not exist", async () => {
      await expect(streamer.connect(sender).extend(999, amount))
        .to.be.revertedWith("TokenStreamer: stream does not exist")
    })

    it("Should not extend stream if caller is not the sender", async () => {
      await expect(streamer.connect(recipient).extend(streamId, amount))
        .to.be.revertedWith("TokenStreamer: caller is not the sender")
    })

    it("Should fail if deposit amount is zero", async () => {
      await expect(streamer.connect(sender).extend(streamId, 0))
        .to.be.revertedWith("TokenStreamer: deposit is too small")
    })

    it("Should fail if deposit amount is too small", async () => {
      await expect(streamer.connect(sender).extend(streamId, paymentRate.div(2)))
        .to.be.revertedWith("TokenStreamer: deposit is too small")
    })

    it("Should fail if deposit amount is not approved", async () => {
      await token.connect(sender).decreaseAllowance(streamer.address, 1)
      await expect(streamer.connect(sender).extend(streamId, amount))
        .to.be.revertedWith("ERC20: transfer amount exceeds allowance")
    })
  })

  describe("Withdrawing funds from stream", () => {
    const deposit = WeiPerEther.mul(20)
    let streamId: BigNumber
    let streamStartTime: number
    let streamDuration: number

    beforeEach(async () => {
      await streamer.register(recipient.address, paymentRate)
      await token.connect(sender).increaseAllowance(streamer.address, deposit)
      const streamCreated = await streamer.connect(sender).create(recipient.address, deposit)
      streamId = await getStreamId(streamCreated)
      const stream = await streamer.getStream(streamId)
      streamStartTime = stream.startTime.toNumber()
      streamDuration = stream.stopTime - stream.startTime
    })

    it("Should withdraw tokens from stream to recipient address", async () => {
      // Middle of the stream
      const timestamp1 = streamStartTime + streamDuration / 2 - 1
      await provider.send("evm_setNextBlockTimestamp", [timestamp1])
      await provider.send("evm_mine", [])
      const amount1 = deposit.div(5)
      await expect(streamer.connect(recipient).withdraw(streamId, amount1))
        .to.emit(streamer, "WithdrawFromStream")
        .withArgs(
          streamId,
          recipient.address,
          amount1,
        )
        .to.emit(streamer, "UpdateSubscription")
        .withArgs(
          sender.address,
          recipient.address,
          streamStartTime + streamDuration,
        )

      expect(await streamer.balanceOf(streamId, sender.address))
        .to.equal(deposit.div(2))
      expect(await streamer.balanceOf(streamId, recipient.address))
        .to.equal(deposit.div(2).sub(amount1))
      expect(await token.balanceOf(streamer.address))
        .to.equal(deposit.sub(amount1))

      // End of the stream
      const timestamp2 = timestamp1 + streamDuration / 2
      await provider.send("evm_setNextBlockTimestamp", [timestamp2])
      await provider.send("evm_mine", [])
      const amount2 = deposit.sub(amount1)
      await expect(streamer.connect(recipient).withdraw(streamId, amount2))
        .to.emit(streamer, "WithdrawFromStream")
        .withArgs(
          streamId,
          recipient.address,
          amount2,
        )
        .to.emit(streamer, "UpdateSubscription")
        .withArgs(
          sender.address,
          recipient.address,
          streamStartTime + streamDuration,
        )

      expect(await streamer.balanceOf(streamId, sender.address)).to.equal(0)
      expect(await streamer.balanceOf(streamId, recipient.address)).to.equal(0)
      expect(await token.balanceOf(streamer.address)).to.equal(0)
    })

    it("Should withdraw tokens from stream to sender address", async () => {
      // Sender withdraws remaining balance and closes the stream
      const timestamp1 = streamStartTime + streamDuration / 2 - 1
      await provider.send("evm_setNextBlockTimestamp", [timestamp1])
      await provider.send("evm_mine", [])
      const expectedSenderBalance = deposit.div(2)
      const expectedRecipientBalance = deposit.sub(expectedSenderBalance)
      const expectedStopTime = (
        streamStartTime + streamDuration -
        expectedSenderBalance.div(paymentRate).toNumber()
      )
      await expect(streamer.connect(sender).withdraw(streamId, expectedSenderBalance))
        .to.emit(streamer, "WithdrawFromStream")
        .withArgs(streamId, sender.address, expectedSenderBalance)
        .to.emit(streamer, "UpdateSubscription")
        .withArgs(
          sender.address,
          recipient.address,
          expectedStopTime,
        )

      expect(await streamer.balanceOf(streamId, sender.address)).to.equal(0)
      expect(await token.balanceOf(streamer.address)).to.equal(expectedRecipientBalance)

      // Recipient withdraws tokens
      expect(await streamer.balanceOf(streamId, recipient.address))
        .to.equal(expectedRecipientBalance)
      await expect(streamer.connect(recipient).withdraw(streamId, expectedRecipientBalance))
        .to.emit(streamer, "WithdrawFromStream")
        .withArgs(streamId, recipient.address, expectedRecipientBalance)
        .to.emit(streamer, "UpdateSubscription")
        .withArgs(
          sender.address,
          recipient.address,
          expectedStopTime,
        )

      expect(await streamer.balanceOf(streamId, sender.address)).to.equal(0)
      expect(await streamer.balanceOf(streamId, recipient.address)).to.equal(0)
      expect(await token.balanceOf(streamer.address)).to.equal(0)
    })

    it("Should fail if stream does not exist", async () => {
      await expect(streamer.connect(sender).withdraw(999, deposit))
        .to.be.revertedWith("TokenStreamer: stream does not exist")
    })

    it("Should not allow 3rd parties to withdaw tokens", async () => {
      await expect(streamer.connect(someone).withdraw(streamId, deposit))
        .to.be.revertedWith("TokenStreamer: invalid address")
    })

    it("Should fail if withdrawal amount is zero", async () => {
      const nextTimestamp = streamStartTime + streamDuration / 2
      await provider.send("evm_setNextBlockTimestamp", [nextTimestamp])
      await provider.send("evm_mine", [])
      await expect(streamer.connect(recipient).withdraw(streamId, 0))
        .to.be.revertedWith("TokenStreamer: withdrawal amount is too small")
    })

    it("Should fail if withdrawal amount is too small", async () => {
      const nextTimestamp = streamStartTime + streamDuration / 2
      await provider.send("evm_setNextBlockTimestamp", [nextTimestamp])
      await provider.send("evm_mine", [])
      await expect(streamer.connect(recipient).withdraw(streamId, paymentRate.div(2)))
        .to.be.revertedWith("TokenStreamer: withdrawal amount is too small")
    })

    it("Should fail if withdrawal amount is not a multiple of rate", async () => {
      const nextTimestamp = streamStartTime + streamDuration / 2
      await provider.send("evm_setNextBlockTimestamp", [nextTimestamp])
      await provider.send("evm_mine", [])
      await expect(streamer.connect(recipient).withdraw(streamId, paymentRate.add(1)))
        .to.be.revertedWith("TokenStreamer: withdrawal amount is not a multiple of rate")
    })

    it("Should not allow withdrawals exceeding remaining balance", async () => {
      const nextTimestamp = streamStartTime + streamDuration / 2
      await provider.send("evm_setNextBlockTimestamp", [nextTimestamp])
      await provider.send("evm_mine", [])
      await expect(streamer.connect(recipient).withdraw(streamId, deposit))
        .to.be.revertedWith("TokenStreamer: amount exceeds available balance")
    })
  })

  describe("Closing streams", () => {
    const deposit = WeiPerEther.mul(20)
    let streamId: BigNumber
    let streamStartTime: number
    let streamDuration: number

    beforeEach(async () => {
      await streamer.register(recipient.address, paymentRate)
      await token.connect(sender).increaseAllowance(streamer.address, deposit)
      const streamCreated = await streamer.connect(sender).create(recipient.address, deposit)
      streamId = await getStreamId(streamCreated)
      const stream = await streamer.getStream(streamId)
      streamStartTime = stream.startTime.toNumber()
      streamDuration = stream.stopTime - stream.startTime
    })

    it("Should let sender close the stream", async () => {
      const timestamp = streamStartTime + streamDuration / 4 - 1
      await provider.send("evm_setNextBlockTimestamp", [timestamp])
      await provider.send("evm_mine", [])
      const expectedSenderBalance = deposit.div(4)
      const expectedRecipientBalance = deposit.sub(expectedSenderBalance)
      await expect(streamer.connect(sender).close(streamId))
        .to.emit(streamer, "WithdrawFromStream")
        .withArgs(streamId, sender.address, expectedSenderBalance)
        .to.emit(streamer, "WithdrawFromStream")
        .withArgs(streamId, recipient.address, expectedRecipientBalance)
        .to.emit(streamer, "UpdateSubscription")
        .withArgs(
          sender.address,
          recipient.address,
          timestamp + 1,
        )

      expect(await streamer.balanceOf(streamId, sender.address)).to.equal(0)
      expect(await streamer.balanceOf(streamId, recipient.address)).to.equal(0)
      expect(await token.balanceOf(streamer.address)).to.equal(0)
    })

    it("Should let recipient close the stream", async () => {
      const timestamp = streamStartTime + streamDuration / 4
      await provider.send("evm_setNextBlockTimestamp", [timestamp])
      await provider.send("evm_mine", [])
      await streamer.connect(recipient).close(streamId)

      expect(await streamer.balanceOf(streamId, sender.address)).to.equal(0)
      expect(await streamer.balanceOf(streamId, recipient.address)).to.equal(0)
      expect(await token.balanceOf(streamer.address)).to.equal(0)
    })

    it("Should not make transfers if stream is already closed", async () => {
      const timestamp = streamStartTime + streamDuration / 4
      await provider.send("evm_setNextBlockTimestamp", [timestamp])
      await provider.send("evm_mine", [])
      await streamer.connect(recipient).close(streamId)
      await expect(streamer.connect(sender).close(streamId))
        .to.emit(streamer, "UpdateSubscription")
        .to.not.emit(streamer, "WithdrawFromStream")
    })

    it("Should not allow 3rd parties to close stream", async () => {
      await expect(streamer.close(streamId))
        .to.be.revertedWith("TokenStreamer: caller is not a participant")
    })

    it("Should disable deposits and close all streams", async () => {
      // Create another stream
      await streamer.register(someone.address, paymentRate)
      await token.connect(sender).increaseAllowance(streamer.address, deposit)
      await streamer.connect(sender).create(someone.address, deposit)

      const timestamp = streamStartTime + streamDuration / 4 - 1
      await provider.send("evm_setNextBlockTimestamp", [timestamp])
      await provider.send("evm_mine", [])
      const expectedSenderBalance = deposit.div(4)
      const expectedRecipientBalance = deposit.sub(expectedSenderBalance)
      await expect(streamer.closeAll())
        .to.emit(streamer, "WithdrawFromStream")
        .withArgs(streamId, sender.address, expectedSenderBalance)
        .to.emit(streamer, "WithdrawFromStream")
        .withArgs(streamId, recipient.address, expectedRecipientBalance)
        .to.emit(streamer, "UpdateSubscription")
        .withArgs(
          sender.address,
          recipient.address,
          timestamp + 1,
        )

      expect(await streamer.balanceOf(streamId, sender.address)).to.equal(0)
      expect(await streamer.balanceOf(streamId, recipient.address)).to.equal(0)
      expect(await token.balanceOf(streamer.address)).to.equal(0)
      expect(await streamer.depositsEnabled()).to.equal(false)
    })

    it("Should not close all streams if caller is not the owner", async () => {
      await expect(streamer.connect(recipient).closeAll())
        .to.be.revertedWith("Ownable: caller is not the owner")
    })
  })

  describe("Changing rate", () => {
    const deposit = WeiPerEther.mul(20)
    const newRate = paymentRate.mul(2)

    beforeEach(async () => {
      await streamer.register(recipient.address, paymentRate)
      await token.connect(sender).increaseAllowance(streamer.address, deposit)
      await streamer.connect(sender).create(recipient.address, deposit)
      await token.connect(anotherSender).increaseAllowance(streamer.address, deposit)
      await streamer.connect(anotherSender).create(recipient.address, deposit)
    })

    it("Should change stream rate", async () => {
      await expect(streamer.changeRate(recipient.address, newRate))
        .to.emit(streamer, "ChangeRate")
        .withArgs(recipient.address, newRate)
      // All streams has been closed
      expect(await token.balanceOf(streamer.address)).to.equal(0)
    })

    it("Should not change stream rate to zero", async () => {
      await expect(streamer.changeRate(recipient.address, 0))
        .to.be.revertedWith("TokenStreamer: payment rate must be greater than zero")
    })

    it("Should not change stream rate if recipient is not registered", async () => {
      await expect(streamer.changeRate(someone.address, newRate))
        .to.be.revertedWith("TokenStreamer: unknown recipient")
    })

    it("Should not change stream rate if caller is not the owner", async () => {
      await expect(streamer.connect(recipient).changeRate(recipient.address, newRate))
        .to.be.revertedWith("Ownable: caller is not the owner")
    })
  })

  describe("ISubscription interface", () => {
    it("Should create and then update stream with send()", async () => {
      const amount1 = WeiPerEther.mul(10)
      const amount2 = WeiPerEther.mul(3)
      await streamer.register(recipient.address, paymentRate)
      await token.connect(sender).increaseAllowance(streamer.address, amount1.add(amount2))
      await expect(streamer.connect(sender).send(recipient.address, amount1))
        .to.emit(streamer, "CreateStream")
        .to.emit(streamer, "UpdateSubscription")
      await expect(streamer.connect(sender).send(recipient.address, amount2))
        .to.emit(streamer, "ExtendStream")
        .to.emit(streamer, "UpdateSubscription")
    })

    it("Should cancel subscription with cancel()", async () => {
      const amount = WeiPerEther.mul(10)
      await streamer.register(recipient.address, paymentRate)
      await token.connect(sender).increaseAllowance(streamer.address, amount)
      await streamer.connect(sender).send(recipient.address, amount)
      await provider.send("evm_mine", [])
      await expect(streamer.connect(sender).cancel(recipient.address))
        .to.emit(streamer, "UpdateSubscription")
    })

    it("Should withdraw with withdrawReceived()", async () => {
      const amount = WeiPerEther.mul(10)
      await streamer.register(recipient.address, paymentRate)
      await token.connect(sender).increaseAllowance(streamer.address, amount)
      await streamer.connect(sender).send(recipient.address, amount)
      await provider.send("evm_mine", [])
      await expect(streamer.connect(recipient).withdrawReceived(sender.address))
        .to.emit(streamer, "UpdateSubscription")
    })

    it("Should withdraw with withdrawReceivedAll()", async () => {
      const amount = WeiPerEther.mul(10)
      await streamer.register(recipient.address, paymentRate)
      await token.connect(sender).increaseAllowance(streamer.address, amount)
      await streamer.connect(sender).send(recipient.address, amount)
      await provider.send("evm_mine", [])
      await expect(streamer.connect(recipient).withdrawReceivedAll())
        .to.emit(streamer, "UpdateSubscription")
    })
  })
})
