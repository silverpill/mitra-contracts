import "@nomiclabs/hardhat-waffle"
import dotenv from "dotenv"

dotenv.config()

const accounts = process.env.WALLET_PRIVATE_KEY ? [process.env.WALLET_PRIVATE_KEY] : []

/**
 * @type import('hardhat/config').HardhatUserConfig
 */
module.exports = {
  paths: {
    tests: "tests",
  },
  networks: {
    localhost: {
      url: "http://127.0.0.1:8546",
    },
    public: {
      url: process.env.ETHEREUM_JSONRPC_URL || "http://127.0.0.1:8545",
      accounts: accounts,
    },
  },
  solidity: "0.8.4",
}
