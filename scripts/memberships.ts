import { ethers } from "hardhat"

async function main() {
  const tokenContractAddress = (
    process.env.MEMBERSHIP_TOKEN_ADDRESS ||
    "0x5FbDB2315678afecb367f032d93F642f64180aa3"
  )
  const [, ...users] = await ethers.getSigners()
  const token = await ethers.getContractAt(
    "MembershipToken",
    tokenContractAddress,
  )
  await token.name()
  for (const user of users) {
    await token.mint(user.address)
    console.log("Minted token to:", user.address)
  }
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error)
    process.exit(1)
  })
