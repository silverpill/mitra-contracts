import { ethers } from "hardhat"
import { WeiPerEther } from "@ethersproject/constants"

async function main() {
  const tokenContractAddress = (
    process.env.PAYMENT_TOKEN_ADDRESS ||
    "0x9fE46736679d2D9a65F0992F2272dE9f3c7fa6e0"
  )
  const [, ...users] = await ethers.getSigners()
  const token = await ethers.getContractAt(
    "PaymentToken",
    tokenContractAddress,
  )
  await token.name()
  const amount = 100
  for (const user of users) {
    await token.transfer(user.address, WeiPerEther.mul(amount))
    console.log("Sent", amount, "tokens to:", user.address)
  }
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error)
    process.exit(1)
  })
