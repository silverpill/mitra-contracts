/**
 * Deploys adapter contract and its dependencies.
 * Environment variables:
 * - CONTRACT_AUTHORITY - address corresponding to server's signing key.
 */
import { ethers } from "hardhat"
import { WeiPerEther } from "@ethersproject/constants"

async function main() {
  const [deployer] = await ethers.getSigners()
  console.log("Deployer:", deployer.address)

  // Default authority is deployer
  const authority = process.env.CONTRACT_AUTHORITY || deployer.address

  const MembershipToken = await ethers.getContractFactory("MembershipToken")
  const membershipToken = await MembershipToken.deploy("Membership token", "ABC")
  console.log("Membership token contract deployed to:", membershipToken.address)

  const Collectible = await ethers.getContractFactory("Collectible")
  const collectible = await Collectible.deploy("Collectible", "NFT")
  console.log("Collectible token contract deployed to:", collectible.address)

  const PaymentToken = await ethers.getContractFactory("PaymentToken")
  const paymentToken = await PaymentToken.deploy("Payment token", "ABC", WeiPerEther.mul(10000))
  console.log("Payment token contract deployed to:", paymentToken.address)

  const Streamer = await ethers.getContractFactory("TokenStreamer")
  const streamer = await Streamer.deploy(paymentToken.address)
  console.log("Token streaming contract deployed to:", streamer.address)

  const Adapter = await ethers.getContractFactory("Adapter")
  const adapter = await Adapter.deploy(
    membershipToken.address,
    collectible.address,
    streamer.address,
    authority,
  )
  console.log("Adapter contract deployed to:", adapter.address)

  await collectible.transferOwnership(adapter.address)
  await streamer.transferOwnership(adapter.address)
  console.log("Ownership transferred to adapter contract")
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error)
    process.exit(1)
  })
