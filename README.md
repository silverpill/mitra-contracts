# Mitra Contracts

Ethereum smart contracts for [Mitra](https://codeberg.org/silverpill/mitra).

Contracts in this repo are provided as examples. Instance administrators could connect arbitrary contracts, as long as there's a single entry point that implements [IGate](./contracts/interfaces/IGate.sol), [IMinter](./contracts/interfaces/IMinter.sol) and [ISubscriptionAdapter](./contracts/interfaces/ISubscriptionAdapter.sol) interfaces.

## Deployment

Deploy contracts to any EVM-compatible chain:

```shell
ETHEREUM_JSONRPC_URL=... WALLET_PRIVATE_KEY=... CONTRACT_AUTHORITY=... npx hardhat run --network public scripts/deploy.ts
```

The script can read environment variables from the `.env` file.

NOTE: Keep your private key safe. Don't copy it to untrusted machines such as cloud servers.

The total cost of deploying default configuration is ~10M gas. [Hardhat console](https://hardhat.org/guides/hardhat-console.html) can be used to deploy custom contracts.

## Interacting with contracts

Launch [Hardhat console](https://hardhat.org/guides/hardhat-console.html):

```shell
ETHEREUM_JSONRPC_URL=... WALLET_PRIVATE_KEY=... npx hardhat console --network public
```

Work with adapter contract:

```javascript
adapter = await ethers.getContractAt('Adapter', '0x...')
```

## Verification

Find contract metadata in `artifacts/build-info` directory:

```shell
ls artifacts/build-info/
```

Open https://sourcify.dev/#/verifier and upload the `.json` file. Select entry point contract and follow instructions.

## Development

### Project setup

```
npm install
npx allow-scripts
```

### Start local ethereum node

```
npm start
```

### Compile

```
npm run compile
```

### Test

```
npm run test
```

### Deploy to the local chain

```
npm run deploy
```

### Mint membership tokens

```
npx hardhat run --network localhost scripts/memberships.ts
```

### Airdrop tokens

```
npx hardhat run --network localhost scripts/airdrop.ts
```
